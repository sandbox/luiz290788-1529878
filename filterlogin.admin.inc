<?php

/**
* @file
* Admin file for module customization
* 
*/

/**
 * Admin settings form.
 */
function filterlogin_general_config() {
  $form = array();
  
  $form[FILTERLOGIN_IPS_SECURED] = array(
    '#type'          => 'textarea',
    '#title'         => t('IPs with permission'),
    '#default_value' => variable_get(FILTERLOGIN_IPS_SECURED, ''),
    '#description'   => t('List here all the IPs that should have access to the secured urls. You can use "%" as a wildcard.')
  );

  $form[FILTERLOGIN_URLS_SECURED] = array(
    '#type'          => 'textarea',
    '#title'         => t('Secured URLs'),
    '#default_value' => variable_get(FILTERLOGIN_URLS_SECURED, ''),
    '#description'   => t('List here all the URLs that should be secured.')
  );

  $form[FILTERLOGIN_PROXY_HEADER] = array(
    '#type'          => 'checkbox',
    '#title'         => t('If you are running behind a load balancer.'),
    '#default_value' => variable_get(FILTERLOGIN_PROXY_HEADER, FALSE),
    '#description'   => t('Loads IP from X-Forwarded-For Header. Please, don\'t enable this if you can access the server ' .
            'from an IP directly. You should only access with the Load Balancer to the server. A forged header is very easy to generate.')
  );
  
  return system_settings_form($form);
}
