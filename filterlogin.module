<?php

/* FORM FIELDS NAMES */
define('FILTERLOGIN_IPS_SECURED', 'filterlogin_ips_secured');
define('FILTERLOGIN_URLS_SECURED', 'filterlogin_urls_secured');
define('FILTERLOGIN_PROXY_HEADER', 'filterlogin_proxy_header');

/**
 * 
 * Implements hook_boot()
 * 
 * Main function. Does the check
 */
function filterlogin_boot() {
  $ips = explode("\n", variable_get(FILTERLOGIN_IPS_SECURED, ''));
  $ips = array_map('trim', $ips);
  $ips = array_filter($ips, 'strlen');

  $urls = variable_get(FILTERLOGIN_URLS_SECURED, '');
  
  if (variable_get(FILTERLOGIN_PROXY_HEADER, FALSE) 
          && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $current_ip = preg_replace('/,.*/', '', $_SERVER['HTTP_X_FORWARDED_FOR']);
  } 
  else {
    $current_ip = ip_address();
  }
  $current_url = $_GET['q'];
  
  include_once DRUPAL_ROOT . '/includes/path.inc';
  if (drupal_match_path($current_url, $urls) && !_filterlogin_check_ip($current_ip, $ips)) {
    /// we should disable the cache, for not caching the access denied and showing swapped
    global $conf;
    $conf['cache'] = FALSE;
    
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    drupal_access_denied();
    // the exit is for disabling the normal render of the page and having two pages rendered
    drupal_exit();
  }
}

/**
 * 
 * Checks if the IP has permission
 * 
 * @param string 	$current_ip	User IP
 * @param array 	$ips				IPs allowed to enter
 * 
 * @return bool 							TRUE if pass the test and FALSE otherwise
 */
function _filterlogin_check_ip($current_ip, $ips) {
  $i = 0;
  $find = FALSE;

  $current_ip_split = explode('.', $current_ip);

  while ($i < count($ips) && $find === FALSE) {
    $checking = $ips[$i];
    if (strpos($checking, '%') !== FALSE) {
      $checking_split = explode('.', $checking);

      $find = TRUE;

      $u = 0;
      while ($u < count($checking_split) && $find === TRUE) {
        $find = $checking_split[$u] == '%' || ($checking_split[$u] == $current_ip_split[$u]);
        $u ++;
      }
    } 
    else {
      // no wildcards
      $find = ($checking == $current_ip);
    }
    
    $i++;
  }

  return $find;
}

/**
 *
 * Implements hook_menu.
 * 
 * @return array 
 */
function filterlogin_menu() {
  $items = array();
  
  $items['admin/config/filterlogin'] = array(
      'title'             => 'Filter login settings',
      'description'       => 'Set general configuration for the module',
      'page callback'     => 'drupal_get_form',
      'page arguments'    => array('filterlogin_general_config'),
      'access arguments'  => array('administer site configuration'),    
      'file'              => 'filterlogin.admin.inc',
      'weight'            => 0,
      'type'              => MENU_NORMAL_ITEM
  );
  
  return $items;
}