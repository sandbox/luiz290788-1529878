/* $Id: README.txt,v 1.30 2011/01/07 00:14:01 sun Exp $ */

-- SUMMARY --

The Filter Login module is used for disabling by the IP specific pages. 
The default parameters just leave open for all IPs but the URLs configured 
are /user, /user/*, /admin/ and /admin/* .

-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/documentation/install/modules-themes/modules-7 for further information.


-- CONFIGURATION --

* Configure Module in Configuration => Filter Login Settings
  - IPs with permission

    In this box you have to enter the IPs allowed. You can use % as a wildcard. 
    Examples: 192.168.120.% for all IPs on that range.

  - Secured URLs

    The URLs that should be secured. Works with q parameter and URL rewrite 
    (?q=/user and /user)

  - If you are running behind a load balancer
   
    Loads IP from X-Forwarded-For header. Please, don't enable this if you can 
    access the server from an IP directly. You should only access with the Load 
    Balancer or Proxy to the server. A forged header is very easy to generate.

-- CONTACT --

Current maintainers:
* Ci&T - http://drupal.org/user/1140410

